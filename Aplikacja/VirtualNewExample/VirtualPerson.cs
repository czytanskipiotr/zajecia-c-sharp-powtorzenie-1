﻿using System;

namespace VirtualNewExample
{
    internal class VirtualPerson
    {
        protected string _name;

        public VirtualPerson(string name)
        {
            _name = name;
        }

        public virtual void SayHello()
        {
            Console.WriteLine($"VIRTUAL - Pracownik {_name} mowi czesc!");
        }
    }

    internal class VirtualManager : VirtualPerson
    {
        public VirtualManager(string name) : base(name)
        {
        }

        public override void SayHello()
        {
            Console.WriteLine($"VIRTUAL - Menadzer {_name} mowi czesc!");
        }
    }
}