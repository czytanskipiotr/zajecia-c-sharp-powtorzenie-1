﻿using System;

namespace VirtualNewExample
{
    internal class NonVirtualPerson
    {
        protected string _name;

        public NonVirtualPerson(string name)
        {
            _name = name;
        }

        public void SayHello()
        {
            Console.WriteLine($"NonVIRTUAL - Pracownik {_name} mowi czesc!");
        }
    }

    internal class NonVirtualManager : NonVirtualPerson
    {
        public NonVirtualManager(string name) : base(name)
        {
        }

        public new void SayHello()
        {
            Console.WriteLine($"NonVIRTUAL - Menadzer {_name} mowi czesc!");
        }
    }
}