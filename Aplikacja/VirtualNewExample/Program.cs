﻿using System;

namespace VirtualNewExample
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Wirtualne:");
            VirtualManager vManager = new VirtualManager("Adam");
            vManager.SayHello();
            VirtualPerson vPerson = vManager;
            vPerson.SayHello();

            Console.WriteLine("Niewirtualne:");
            NonVirtualManager nvManager = new NonVirtualManager("Adam");
            nvManager.SayHello();
            NonVirtualPerson nvPerson = nvManager;
            nvPerson.SayHello();
            Console.ReadKey();
        }
    }
}