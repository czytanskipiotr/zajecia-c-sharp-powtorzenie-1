﻿using System;
using System.Linq;
using Aplikacja;
using Aplikacja.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace PowtorzenieTesty
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void LatitudeConstructorOK()
        {
            var lat = new Latitude(5.0f, WorldDirection.North);
            Assert.AreEqual(5.0f, lat.Degree);
            Assert.AreEqual(WorldDirection.North, lat.Direction);
        }

        [TestMethod]
        public void LatitudeConstructorIncorrectDegree()
        {
            Assert.ThrowsException<ArgumentException>(() => new Latitude(-5.0f, WorldDirection.North));
            Assert.ThrowsException<ArgumentException>(() => new Latitude(91.0f, WorldDirection.North));
        }

        [TestMethod]
        public void LatitudeConstructorIncorrectDirection()
        {
            Assert.ThrowsException<ArgumentException>(() => new Latitude(15.0f, WorldDirection.East));
            Assert.ThrowsException<ArgumentException>(() => new Latitude(85.0f, WorldDirection.West));
        }

        [TestMethod]
        public void LatitudeToStringTest()
        {
            var degree = 25.0f;
            var direction = WorldDirection.North;
            Assert.AreEqual($"{degree} {direction}", new Latitude(degree, direction).ToString());
        }

        [TestMethod]
        public void LongtitudeConstructorOK()
        {
            var longt = new Longtitude(151.0f, WorldDirection.East);
            Assert.AreEqual(151.0f, longt.Degree);
            Assert.AreEqual(WorldDirection.East, longt.Direction);
        }

        [TestMethod]
        public void LongtitudeConstructorIncorrectDegree()
        {
            Assert.ThrowsException<ArgumentException>(() => new Longtitude(-5.0f, WorldDirection.East));
            Assert.ThrowsException<ArgumentException>(() => new Longtitude(181.0f, WorldDirection.West));
        }

        [TestMethod]
        public void LongtitudeConstructorIncorrectDirection()
        {
            Assert.ThrowsException<ArgumentException>(() => new Longtitude(150.0f, WorldDirection.North));
            Assert.ThrowsException<ArgumentException>(() => new Latitude(91.0f, WorldDirection.South));
        }

        [TestMethod]
        public void LongtitudeToStringTest()
        {
            var degree = 15.0f;
            var direction = WorldDirection.East;
            Assert.AreEqual($"{degree} {direction}", new Longtitude(degree, direction).ToString());
        }

        [TestMethod]
        public void AttachAndNotifyTest()
        {
            var subject = new Location(Location.DefaultLocation);
            var testObserver = new TestObserver();
            var testObserver2 = new TestObserver();
            subject.Attach(testObserver);
            subject.Attach(testObserver2);
            subject.Latitude = new Latitude(25.0f, WorldDirection.North);
            subject.Latitude = new Latitude(15.0f, WorldDirection.South);
            subject.Longtitude = new Longtitude(26.0f, WorldDirection.East);
            subject.Longtitude = new Longtitude(54.0f, WorldDirection.West);
            Assert.AreEqual(4, testObserver.Counter);
            Assert.AreEqual(4, testObserver2.Counter);
        }

        [TestMethod]
        public void DeleteObserverTest()
        {
            var subject = new Location(Location.DefaultLocation);
            var testObserver = new TestObserver();
            var testObserver2 = new TestObserver();
            subject.Attach(testObserver);
            subject.Attach(testObserver2);
            subject.Latitude = new Latitude(25.0f, WorldDirection.North);
            subject.Latitude = new Latitude(15.0f, WorldDirection.South);
            subject.Delete(testObserver2);
            subject.Longtitude = new Longtitude(26.0f, WorldDirection.East);
            subject.Longtitude = new Longtitude(54.0f, WorldDirection.West);
            Assert.AreEqual(4, testObserver.Counter);
            Assert.AreEqual(2, testObserver2.Counter);
        }

        [TestMethod]
        public void LocationHistoryCountTest()
        {
            var subject = new Location(Location.DefaultLocation);
            var locationHistory = new LocationHistory(subject);
            subject.Latitude = new Latitude(25.0f, WorldDirection.North);
            subject.Latitude = new Latitude(15.0f, WorldDirection.South);
            subject.Longtitude = new Longtitude(26.0f, WorldDirection.East);
            subject.Longtitude = new Longtitude(54.0f, WorldDirection.West);
            Assert.AreEqual(4, locationHistory.History.Count);
        }

        [TestMethod]
        public void LocationHistoryContentTest()
        {
            var subject = new Location(Location.DefaultLocation);
            var locationHistory = new LocationHistory(subject);
            subject.Latitude = new Latitude(25.0f, WorldDirection.North);
            subject.Latitude = new Latitude(15.0f, WorldDirection.South);
            subject.Longtitude = new Longtitude(26.0f, WorldDirection.East);
            subject.Longtitude = new Longtitude(54.0f, WorldDirection.West);
            var history = locationHistory.History;
            Assert.AreEqual(1,
                history.Select(x => x.Item1).Count(x =>
                    x.Longtitude.Degree == 54.0f && x.Longtitude.Direction == WorldDirection.West));
        }

        private class TestObserver : IObserver
        {
            public int Counter;

            public void Update()
            {
                Counter++;
            }
        }
    }
}