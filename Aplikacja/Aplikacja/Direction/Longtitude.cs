﻿using System;

namespace Aplikacja
{
    public struct Longtitude
    {
        public Longtitude(float longtitude, WorldDirection direction)
        {
            if (direction != WorldDirection.West && direction != WorldDirection.East || longtitude < 0.0f ||
                longtitude > 180.0f)
                throw new ArgumentException();
            Degree = longtitude;
            Direction = direction;
        }

        public float Degree { get; }

        public WorldDirection Direction { get; }

        public override string ToString()
        {
            
            return $"{Degree} {Direction}";

        }
    }
}