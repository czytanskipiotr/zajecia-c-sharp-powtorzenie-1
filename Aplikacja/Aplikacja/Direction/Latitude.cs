﻿using System;

namespace Aplikacja
{
    public struct Latitude
    {
        public Latitude(float latitude, WorldDirection direction)
        {
            if (direction != WorldDirection.North && direction != WorldDirection.South || latitude < 0.0f ||
                latitude > 90.0f)
                throw new ArgumentException();
            Degree = latitude;
            Direction = direction;
        }

        public float Degree { get; }

        public WorldDirection Direction { get; }

        public override string ToString()
        {
            return $"{Degree} {Direction}";
        }
    }
}