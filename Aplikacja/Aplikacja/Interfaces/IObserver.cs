﻿namespace Aplikacja.Interfaces
{
    public interface IObserver
    {
        void Update();
    }
}