﻿namespace Aplikacja.Interfaces
{
    public interface ISubject
    {
        void Attach(IObserver observer);
        void Delete(IObserver observer);
        void NotifyObservers();
    }
}