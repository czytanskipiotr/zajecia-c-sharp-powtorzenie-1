﻿using System.Collections.Generic;
using Aplikacja.Interfaces;

namespace Aplikacja
{
    public class Location : ISubject
    {
        public static Location DefaultLocation = new Location(new Latitude(0.0f, WorldDirection.North),
            new Longtitude(0.0f, WorldDirection.East));

        private readonly List<IObserver> _observers;

        private Latitude _latitude;
        private Longtitude _longtitude;

        public Location(Latitude latitude, Longtitude longtitude)
        {
            _latitude = latitude;
            _longtitude = longtitude;
            _observers = new List<IObserver>();
        }

        public Location(Location previousLocation) : this(previousLocation.Latitude, previousLocation.Longtitude)
        {
            
         
        }

        public Latitude Latitude
        {
            get => _latitude;
            set
            {
                _latitude = value;
                NotifyObservers();
            }
        }

        public Longtitude Longtitude
        {
            get => _longtitude;
            set
            {
                _longtitude = value;
                NotifyObservers();
            }
        }

        public void Attach(IObserver observer)
        {
            if (_observers.Contains(observer)) return; 
            _observers.Add(observer);
        }

        public void Delete(IObserver observer)
        {
            _observers.Remove(observer);
        }

        public void NotifyObservers()
        {
            _observers.ForEach(x => x.Update());
        }

        public override string ToString()
        {
            return $"{Latitude} {Longtitude}";
        }
    }
}