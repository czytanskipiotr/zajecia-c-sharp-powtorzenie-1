﻿using System;
using Aplikacja.Interfaces;

namespace Aplikacja
{
    internal class ConsoleLogger : IObserver
    {
        private readonly ISubject _subject;

        public ConsoleLogger(ISubject subject)
        {
            _subject = subject;
            _subject.Attach(this);
        }

        public void Update()
        {
            Console.WriteLine("====================================================");
            Console.WriteLine($"=>{_subject}");
            Console.WriteLine("====================================================");
        }
    }
}