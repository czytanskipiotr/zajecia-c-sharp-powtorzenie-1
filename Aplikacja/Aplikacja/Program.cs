﻿using System;

namespace Aplikacja
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var location = new Location(new Latitude(15.0f, WorldDirection.North),
                new Longtitude(16.7f, WorldDirection.West));
            var locationHistory = new LocationHistory(location);
            var logger = new ConsoleLogger(location);
            location.Latitude = new Latitude(17.0f, WorldDirection.North);
            location.Longtitude = new Longtitude(25.0f, WorldDirection.East);
            location.Latitude = new Latitude(12.0f, WorldDirection.South);
            var history = locationHistory.History;
            foreach (var element in history) Console.WriteLine($"{element.Item2} => {element.Item1}");
            Console.ReadKey();
        }
    }
}